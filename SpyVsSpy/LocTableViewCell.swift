//
//  LocTableViewCell.swift
//  SpyVsSpy
//
//  Created by Candicane on 11/23/17.
//  Copyright © 2017 Joseph Hinkle. All rights reserved.
//

import UIKit

class LocTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label_text: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
