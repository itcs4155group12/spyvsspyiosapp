# README #

This README would normally document whatever steps are necessary to get your application up and running.

Spy vs Spy Project Proposal
ITCS-4155-001 
Group 12
The Problem: 
We have identified two major problems for incoming freshman. First, they are unfamiliar with the campus layout and don’t know the building names or their where they are located. Second, they may struggle to make friends or meet new people as most of their previous friends were in high school. We believe that a fun, mobile game can solve both of these problems through actively engaging these students to get to know each other and their campus.

The Solution and Description:
Spy vs. Spy is an augmented reality game. At the start of each game, seven players will meet at one location at an assigned time. The players will meet each other, verify that they are real students (by scanning UNCC ID cards) and collect contact information. The following school day the game begins. Two players will be randomly assigned the role of “sniper” in secret. The other five players have no knowledge as to which players are the snipers. Their goal is to “hack” 50 buildings before the end of the school week. Meanwhile the snipers can pretend that they are fellow hacker spies and gain trust. Eventually once a sniper spy judges it to be a strategic moment, he or she can take a photo of a hacker spy and “stun” them. This triggers a voting process where the players can vote whether or not the stun was valid or not. If the stun is determined to be invalid, the hacker can continue on their way. If it is valid, the hacker loses and is removed from the game. The sniper spies win by shooting all the hacker spies before the end of the week. The hacker spies win by hacking 50 buildings before the end of the week. 
To hack a building, a hacker spy touches their NFC-enabled Android phone to an access point on the building. This will then send a message to our game server that the player is hacking the access point at that location. This means that we can then push that information to all players. Thus, when the hacker Sally hacks the building “Fretwell,” all players, including the snipers, will get the notification “Sally is hacking Fretwell.”
We are also taking a minimal design approach. Since, we want students to both learn the campus and meet new students, we are not including an in-app map. Instead, users will be given location information in the form of building titles. For example, if a student needs to locate Burson, rather than give them a digital map with a pin on Burson, we will instead simply display text such as “go to Burson.” This forces freshman to learn where Burson is located through conventional means. 

Targeted Audience:
The target audience for this app are mainly UNC Charlotte freshmen since they are the ones who will benefit the most from using the features provided by the app. As stated earlier, freshmen often are not familiar with their university campus or struggle with meeting new people/making new friends and this app would give them a platform to do both of those things at once. Along with freshmen, any UNC Charlotte student would be able and welcome to play this game and these students would benefit from it as well: they would be able to meet new people and hopefully make new friends. 

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Normal XCode setup
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
By Candace Allison, Megan Reiffer and Joseph Hinkle
* Other community or team contact
